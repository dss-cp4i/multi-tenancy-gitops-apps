apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: git-clone-shared-directories
spec:
  params:
    - name: repo-url
      type: string
      description: "SSH URL of the repository to clone from."
    - name: revision
      type: string
      default: "main"
      description: "The git revision to checkout (branch, tag, sha, commit ID, etc)."
    - name: directories
      type: string
      description: "Space-separated list of directories to search for BAR files."
  workspaces:
    - name: output
      description: "The workspace to clone the repo into and prepare BAR files."
    - name: ssh-credentials
      description: "The workspace for SSH credentials."
  steps:
    - name: setup-ssh
      image: alpine/git
      script: |
        #!/bin/sh
        set -eu
        SSH_CREDS_PATH="$(workspaces.ssh-credentials.path)"
        mkdir -p ~/.ssh
        cp "$SSH_CREDS_PATH"/* ~/.ssh/
        chmod 700 ~/.ssh
        chmod 600 ~/.ssh/*
        ssh-keyscan -H github.com >> ~/.ssh/known_hosts
        ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts

    - name: clone-and-checkout
      image: alpine/git
      script: |
        #!/bin/sh
        set -eu
        REPO_URL="$(params.repo-url)"
        REVISION="$(params.revision)"
        OUTPUT_PATH="$(workspaces.output.path)"
        
        # Initialize Git repository
        git init "$OUTPUT_PATH"
        cd "$OUTPUT_PATH"

        # Setup or update remote origin
        if git remote | grep -q origin; then
          git remote set-url origin "$REPO_URL"
        else
          git remote add origin "$REPO_URL"
        fi

        git config core.sparseCheckout true
        
        # Prepare for sparse checkout
        for dir in $(echo "$(params.directories)" | tr " " "\n"); do
          echo "$dir/*" >> .git/info/sparse-checkout
        done
        
        git fetch --depth=1 origin "$REVISION"
        git checkout FETCH_HEAD
