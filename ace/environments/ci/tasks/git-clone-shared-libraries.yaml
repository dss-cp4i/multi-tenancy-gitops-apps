apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: git-clone-and-prepare-bar
spec:
  params:
    - name: repo-url
      type: string
      description: "SSH URL of the repository to clone from"
    - name: revision
      type: string
      default: "main"
      description: "The git revision to checkout (branch, tag, sha, commit ID, etc)"
    - name: directories
      type: string
      description: "Space-separated list of directories to search for BAR files"
  workspaces:
    - name: output
      description: "The workspace to clone the repo into and prepare BAR files"
    - name: ssh-credentials
      description: "The workspace for SSH credentials"
  steps:
    - name: setup-ssh
      image: alpine/git
      script: |
        #!/bin/sh
        set -eu
        SSH_CREDS_PATH="$(workspaces.ssh-credentials.path)"

        # Set up SSH credentials
        mkdir -p ~/.ssh
        cp "$SSH_CREDS_PATH"/* ~/.ssh/
        chmod 700 ~/.ssh
        chmod 600 ~/.ssh/*

        # Configure SSH known hosts
        ssh-keyscan -H github.com >> ~/.ssh/known_hosts
        ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts

    - name: clone-and-checkout
      image: alpine/git
      script: |
        #!/bin/sh
        set -eu
        OUTPUT_PATH="$(workspaces.output.path)"
        REPO_URL="$(params.repo-url)"
        REVISION="$(params.revision)"

        # Clone and checkout
        git clone "$REPO_URL" "$OUTPUT_PATH"
        cd "$OUTPUT_PATH"
        git checkout "$REVISION"

    - name: sparse-checkout-and-prepare-bar
      image: alpine/git
      script: |
        #!/bin/sh
        set -eu
        OUTPUT_PATH="$(workspaces.output.path)"
        DIRECTORIES="$(params.directories)"
        
        cd "$OUTPUT_PATH"
        git config core.sparseCheckout true
        
        # Handle directories for sparse checkout
        echo "" > .git/info/sparse-checkout
        for dir in $DIRECTORIES; do
          echo "$dir/*" >> .git/info/sparse-checkout
        done
        
        git read-tree -mu HEAD

        # Assuming BAR files are directly under the specified directories
        # Find BAR files and prepare them for build
        find $DIRECTORIES -type f -name "*.bar" -exec cp {} $OUTPUT_PATH \;

      securityContext:
        runAsUser: 0

